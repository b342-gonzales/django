from django import forms
from django.contrib.auth.hashers import check_password, make_password

class RegisterForm(forms.Form):
	username = forms.CharField(label="Username", max_length=20)
	first_name = forms.CharField(label="First Name", max_length=50)
	last_name = forms.CharField(label="Last Name", max_length=50)
	email = forms.EmailField(label="Email", max_length=20)
	password = forms.CharField(label="Password", max_length=20, widget=forms.PasswordInput)
	confirm_password = forms.CharField(label="Confirm Password", max_length=20, widget=forms.PasswordInput)

	def clean(self):
		cleaned_data = super().clean()
		password = cleaned_data.get("password")
		confirm_password = cleaned_data.get("confirm_password")

		if password != confirm_password:
			raise forms.ValidationError("Passwords do not match.")

		return cleaned_data

class LoginForm(forms.Form):
	username = forms.CharField(label="Username", max_length=20)
	password = forms.CharField(label="Password", max_length=20)


class AddTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	status = forms.CharField(label='Status', max_length=50)

class AddEventForm(forms.Form):
	event_name = forms.CharField(label="Event Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)

class UpdateEventForm(forms.Form):
	event_name = forms.CharField(label="Event Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	status = forms.CharField(label='Status', max_length=50)

class UpdateUserForm(forms.Form):
	first_name = forms.CharField(label="First Name", max_length=50, required=False)
	last_name = forms.CharField(label="Last Name", max_length=50, required=False)
	password = forms.CharField(label="Password", max_length=20, widget=forms.PasswordInput, required=False)
	confirm_password = forms.CharField(label="Confirm Password", max_length=20, widget=forms.PasswordInput, required=False)

