from django.urls import path
from . import views
app_name = 'todolist'
urlpatterns = [
	path('', views.index, name = 'index'),
	# /todoitem/<todoitem_id>
	path('todoitem/<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
	# /eventitem/<eventitem_id>
	path('eventitem/<int:eventitem_id>/', views.eventitem, name='vieweventitem'),
	# /todolist/register
	path('register', views.register, name="register"),
	# /todolist/change_password
	path('change_password', views.change_password, name="change_password"),
	# /todolist/login
	path('login', views.login_view, name="login"),
	# todolist/logout
	path('logout', views.logout_view, name="logout"),
	# /todolist/add_task
	path('add_task', views.add_task, name="add_task"),
	# /todolist/todoitem_id/edit
	path('todoitem/<int:todoitem_id>/update_task', views.update_task, name='update_task'),
	# /todolist/todoitem_id/delete
	path('todoitem/<int:todoitem_id>/delete', views.delete_task, name='delete_task'),
	# /todolist/add_event
	path('add_event', views.add_event, name="add_event"),
	# /todolist/eventitem_id/edit
	path('eventitem/<int:eventitem_id>/update_event', views.update_event, name='update_event'),
	# /todolist/eventitem_id/delete
	path('eventitem/<int:eventitem_id>/delete', views.delete_event, name='delete_event'),
	# /todolist/update_user
	path('update_user/', views.update_user, name='update_user')

]